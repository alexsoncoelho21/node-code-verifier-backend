# Welcome to Node-code-verifier

## Dependencies

- express - server
- nodemon - automatically restarts node app
- concurrently - runs commands concurrently

## Scripts

- `npm run build` - builds for
- `npm run start` - runs trsnapiled JS
- `npm run dev` - starts development server
- `npm run test` - runs unit tests
- `serve:coverage` - generates coverage report and serves html content

## Set up local Environment

- `npm run install ` to install dependencies.
- on your terminal root project run `cp .env.example .env` and replace values.

## Local connection to db

`mongod --dbpath <pathtodb>`
