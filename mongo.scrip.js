db.Contacts.find({"location.country": "Germany"}).limit(1)
db.Contacts.find({"name": "Blake"})
db.Contacts.find({}).sort({name:1}).limit(4)
db.Contacts.aggregate([{$out: "CopiaContact"}])
db.CopiaContact.drop()
db.Contacts.updateMany({}, {$rename: {name: "nombre"}})
db.Contacts.deleteMany({"location.state": "Florida"})

db.Contacts.find({"location.city": {$regex : /^A/i}}).sort({"location.city":-1}).limit(5)
db.Contacts.find("dob.age": {$gte: 18, $lte: 50 })
db.Contacts.find({favNumber: {$exists : true }}).forEach(function(Contacts) {
  db.Contacts.updateOne({_id: Contacts._id}, {$set: {favNumber: Math.floor(Math.random() * 1000)}})
})


//* Bucket *\\
db.Contacts.aggregate([ { $bucket: {
       groupBy: '$favNumber',
       boundaries: [100,200,300,400,500,600,700,800,900],
       default: 'Other',
       output: {
           count: { $sum: 1},
           email: {$addToSet: "$email"}
       }
} }
])


//* Projection *\\
{"nombre.first": 1, "login.username": 1}


db.Contacts.aggregate([
   { $group: { _id: "nombres", name: {$addToSet: "$nombre.first"
}}},
   { $unwind: "$name" },
   { $sort: { "nombre.last" : 1 }}
])

new Date("1953-12-21T17:26:40.246Z").toLocaleString()



//* WIP Date transformation *\\
db.Contacts.aggregate([
   { $project: {
       _id: 0,
       fechaNacimiento:
       { $concat: [
           {$toString: new Date("$dob.date")},
           "-",
           {$toString: "$dob.date"},
           "-",
           {$toString: "$dob.date"}
       ]
}}} ])