import { GoodbyeController } from "../controller/GoodbyeController";
import express, { Request, Response } from "express";
import { LogInfo } from "../utils/logger";
import { GoodbyeResponse } from "../controller/types";

// Router from express
let goobyeRouter = express.Router();

// http://localhost:8000/api/hello?name=Alex/
goobyeRouter
  .route("/")
  // GET:
  .get(async (req: Request, res: Response) => {
    // Obtain a Query Param
    let name: any = req?.query?.name;
    LogInfo(`Query Param: ${name}`);
    // Controller Instance to execute method
    const controller: GoodbyeController = new GoodbyeController();
    // Obtain Response
    const response: GoodbyeResponse = await controller.getMessage(name);
    //   Send to client the response
    return res.send(response);
  });

export default goobyeRouter;
