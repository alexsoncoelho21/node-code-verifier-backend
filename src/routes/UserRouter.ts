import { UsersController } from "../controller/UsersController";
import express, { Request, Response } from "express";
import { LogInfo } from "../utils/logger";

// Router from express
let usersRouter = express.Router();

// http://localhost:8000/api/users?id=63b350cb77707fa1f293b85c
usersRouter
  .route("/")
  // GET:
  .get(async (req: Request, res: Response) => {
    let id: any = req?.query?.id;
    LogInfo(`Query Param: ${id}`);
    // Controller Instance to execute method
    const controller: UsersController = new UsersController();
    const response: any = await controller.getUsers(id);
    //   Send to client the response
    return res.json(response);
  })
  // Delete
  .delete(async (req: Request, res: Response) => {
    let id: any = req?.query?.id;
    LogInfo(`Query Param: ${id}`);
    const controller: UsersController = new UsersController();
    const response: any = await controller.deleteUser(id);
    return res.json(response);
  })
  // POST
  .post(async (req: Request, res: Response) => {
    let name: any = req?.query?.name;
    let age: any = req?.query?.age;
    let email: any = req?.query?.email;

    let user = {
      name,
      age,
      email,
    };
    const controller: UsersController = new UsersController();
    const response: any = await controller.createUser(user);
    return res.json(response);
  })
  // PUT
  .put(async (req: Request, res: Response) => {
    let id: any = req?.query?.id;
    let name: any = req?.query?.name;
    let age: any = req?.query?.age;
    let email: any = req?.query?.email;

    let user = {
      name,
      age,
      email,
    };
    const controller: UsersController = new UsersController();
    const response: any = await controller.updateUser(id, user);
    return res.json(response);
  });

export default usersRouter;
