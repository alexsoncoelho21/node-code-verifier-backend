/**
 * Root Router
 * Redirections to Routers
 */

import express, { Request, Response } from "express";
import helloRouter from "./HelloRouter";
import { LogInfo } from "../utils/logger";
import goobyeRouter from "./GoodbyeRouter";
import usersRouter from "./UserRouter";

// Server instance
let server = express();

// Router instance
let rootRouter = express.Router();

// Activate for requests to http://localhost:8000/api

// GET: http://localhost:8000/api/
rootRouter.get("/", (req: Request, res: Response) => {
  LogInfo("GET: http://localhost:8000/api/");
  // Send Hello World
  res.send("APP Express + TS + Swagger + Mongoose + Nodemon + Jest");
});

// Redirections to Routers & Controllers
server.use("/", rootRouter);
server.use("/hello", helloRouter);
server.use("/goodbye", goobyeRouter);
// Add more routers
server.use("/users", usersRouter);

export default server;
