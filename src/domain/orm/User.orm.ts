import { userEntity } from "../entities/User.entity";

import { LogError, LogSuccess, LogWarning } from "../../utils/logger";

// CRUD

/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 * @returns
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {
  try {
    let userModel = userEntity();
    LogSuccess(`[ORM SUccess]: Getting all Users`);
    const users = await userModel.find({}).exec();
    return users;
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all Users: ${error}`);
  }
};

/**
 * Method to obtain a user by Id
 * @param id The id of the user
 * @returns a user from "Users" collection
 */
export const getUserById = async (id: string): Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    const user = userModel.findById(id);
    return user;
  } catch (error) {
    LogError(`[ORM ERROR]: Getting user by ID ${id}: ${error}`);
  }
};

/**
 * Method to delete a user by Id
 * @param id The id of the user
 * @returns a message with result of user deletion.
 */
export const deleteUser = async (id?: string): Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    return await userModel.deleteOne({ _id: id });
  } catch (error) {
    LogError(`[ORM ERROR]: Deleting user with ${id}: ${error}`);
  }
};

/**
 * Method to create a new user
 * @param user the user to be created
 * @returns a message with result of user creation.
 */
export const createUser = async (user: any): Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    return await userModel.create(user);
  } catch (error) {
    LogError(`[ORM ERROR]: Creating user: ${error}`);
  }
};

/**
 * Method to create a new user
 * @param user the user to be created
 * @returns a message with result of user creation.
 */
export const updateUser = async (
  id: string,
  user: any
): Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    return await userModel.findByIdAndUpdate(id, user);
  } catch (error) {
    LogError(`[ORM ERROR]: Updating user with id ${id}: ${error}`);
  }
};
