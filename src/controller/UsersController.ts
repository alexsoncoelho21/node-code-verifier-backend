import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { BasicResponse } from "./types";
import { IHelloController, IUserController } from "./interfaces";
import { LogSuccess } from "../utils/logger";

// ORM - Users Collection
import {
  deleteUser,
  getAllUsers,
  getUserById,
  createUser,
  updateUser,
} from "../domain/orm/User.orm";
import { response } from "express";

@Route("/api/users")
@Tags("UserController")
export class UsersController implements IUserController {
  /**
   * Endpoint to retrieve the Users in the collection "Users" from DB
   * @param {string} id Id of user to retrieve
   * @returns All users or user by found by Id
   */
  @Get("/")
  public async getUsers(@Query() id?: string): Promise<any> {
    if (id) {
      LogSuccess(`[/api/users] Get User By ID: ${id}`);
      return await getUserById(id);
    } else {
      LogSuccess("[/api/users] Get All Users Request");
      return await getAllUsers();
    }
  }

  /**
   * Endpoint to delete user by Id from DB
   * @param {string} id Id of user to delete
   * @returns Success message if user was deleted
   */
  @Delete("/")
  public async deleteUser(@Query() id?: string): Promise<any> {
    let response: any = "";

    if (id) {
      await deleteUser(id).then((resp) => {
        LogSuccess(`[/api/users] Deleting User By ID: ${id}`);
        response = {
          message: `User with Id ${id} deleted`,
        };
      });
    } else {
      response = {
        message: `Please provide a user Id`,
      };
    }

    return response;
  }

  @Post("/")
  public async createUser(user: any): Promise<any> {
    let response: any = "";

    await createUser(user).then((resp) => {
      LogSuccess(`[/api/users] Creating User with name: ${user?.name}`);
      response = {
        message: `User created ${user?.name}`,
      };
    });

    return response;
  }

  @Put("/")
  public async updateUser(id: string, user: any): Promise<any> {
    let response: any = "";

    if (id) {
      await updateUser(id, user).then((resp) => {
        LogSuccess(`[/api/users] Updating User with id: ${id}`);
        response = {
          message: `User updated with Id ${id}`,
        };
      });
    } else {
      response = {
        message: `Please provide a user Id to be updated`,
      };
    }

    return response;
  }
}
