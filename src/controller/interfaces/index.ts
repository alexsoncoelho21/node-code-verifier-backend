import { BasicResponse, GoodbyeResponse } from "../types";

export interface IHelloController {
  getMessage(name?: string): Promise<BasicResponse>;
}

export interface IGoodbyeController {
  getMessage(name?: string): Promise<GoodbyeResponse>;
}

export interface IUserController {
  // Read all users from database or User by Id
  getUsers(id?: string): Promise<any>;
  // Delete user by ID
  deleteUser(id: string): Promise<any>;
  // Create new user
  createUser(user: any): Promise<any>;
}
