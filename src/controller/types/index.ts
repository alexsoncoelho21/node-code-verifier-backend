/**
 * Basic JSON response for Controllers
 */
export type BasicResponse = {
  message: string;
};

/**
 * Error response for Controllers
 */
export type ErrorResponse = {
  error: string;
  message: string;
};

/**
 * JSON response for Goodbye Controller
 */
export type GoodbyeResponse = {
  message: string;
  date: Date;
};
